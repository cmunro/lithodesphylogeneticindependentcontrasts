#!/bin/bash

# Request an hour of runtime:
#SBATCH --time=24:00:00

#SBATCH --nodes=1
#SBATCH --tasks-per-node=8

# Specify a job name:
#SBATCH -J MyMPIJob

# Specify an output file
#SBATCH -o Mafft_full-%j.out
#SBATCH -e Mafft_full-%j.out

module load mafft

#mafft --maxiterate 1000 --localpair 16S.full_rearranged.fasta > 16S.full_rearranged.aligned.fasta
#mafft --maxiterate 1000 --localpair 28S.full_rearranged.fasta > 28S.full_rearranged.aligned.fasta
mafft --maxiterate 1000 --localpair COI.full_rearranged.fasta > COI.full_rearranged2.aligned.fasta
#mafft --maxiterate 1000 --localpair ITS1.full.fasta > ITS1.full.aligned.fasta
