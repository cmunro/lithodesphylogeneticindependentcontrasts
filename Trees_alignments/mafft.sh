#!/bin/bash

# Request an hour of runtime:
#SBATCH --time=24:00:00

#SBATCH --nodes=1
#SBATCH --tasks-per-node=8

# Specify a job name:
#SBATCH -J MyMPIJob

# Specify an output file
#SBATCH -o Mafft-%j.out
#SBATCH -e Mafft-%j.out

module load mafft

mafft --maxiterate 1000 --localpair 12S.fasta > 12S.aligned.fasta
mafft --maxiterate 1000 --localpair 16S.fasta > 16S.aligned.fasta
mafft --maxiterate 1000 --localpair 28S.fasta > 28S.aligned.fasta
mafft --maxiterate 1000 --localpair COI.fasta > COI.aligned.fasta
