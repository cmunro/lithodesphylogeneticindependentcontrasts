#!/bin/bash

# Request an hour of runtime:
#SBATCH --time=24:00:00

#SBATCH --nodes=1
#SBATCH --tasks-per-node=8

# Specify a job name:
#SBATCH -J MyMPIJob

# Specify an output file
#SBATCH -o MyMPIJob-%j.out
#SBATCH -e MyMPIJob-%j.out

# Run a command

module load raxml

mpirun -n 8 raxmlHPC-MPI  -f a -x 12345 -p 12345 -N 1000 -m GTRGAMMA -s combined.phy -n combined_boot1000
mpirun -n 8 raxmlHPC-MPI  -f a -x 12345 -p 12345 -N 1000 -m GTRGAMMA -s 28S.aligned.fasta.phy -n 28s_boot1000
mpirun -n 8 raxmlHPC-MPI  -f a -x 12345 -p 12345 -N 1000 -m GTRGAMMA -s 16S.aligned.fasta.phy -n 16s_boot1000
mpirun -n 8 raxmlHPC-MPI  -f a -x 12345 -p 12345 -N 1000 -m GTRGAMMA -s COI.aligned.fasta.phy -n COI_boot1000
#mpirun -n 8 raxmlHPC-MPI  -f a -x 12345 -p 12345 -N 1000 -m GTRGAMMA -s 12S.aligned.fasta -n 12s_boot1000
