##README##

This repository contains all of the code used to generate lithodid phylogenies that were used to carry out phylogenetic independent contrasts on zoea I/larval duration and dry mass data for Brown et al. 2018. 

The final tree used for this analysis can be found in `/Trees_alignments` with the name `RAxML_bipartitions.combined_boot1000`. 

All of the code for these analyses can be found in `contrasts.Rmd`. Additionally, the results of these analyses, including figures and trees, can be found in `contrasts.pdf`.